#! /bin/bash 

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

#run picom -b 
run nitrogen --restore 
run flameshot 
run gwe --hide-window 
run thunar --daemon 
run nmcli connection up gentoo 
run nm-applet 
run blueman-applet 
run dunst 
run lxpolkit 
run discord 
run spotify 
run kitty -e gotop 
run xsetwacom set 16 MapToOutput HEAD-0 && 
	xsetwacom set 16 area 0 0 15750 8859 &&
	xsetwacom set 16 Suppress 1 && 
	xsetwacom set 16 RawSample 1

export _JAVA_AWT_WM_NONREPARENTING=1

while true; do
	xsetroot -name " [vol $(pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(( $SINK + 1 )) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,')%] $(date +"[%x] [%I:%M %P] ")"
	sleep 0.1s
done &
