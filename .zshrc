### EXPORTS ###
export ZSH="/home/dasprii/.oh-my-zsh"
export VISUAL=vim
export EDITOR=vim

### THEME ###
ZSH_THEME="bureau"

### PLUGINS ###
plugins=(
  git
  urltools
  bgnotify
  zsh-autosuggestions
  zsh-syntax-highlighting
  dotbare
)

source $ZSH/oh-my-zsh.sh
source /usr/share/fzf/key-bindings.zsh

### ALIASES ###
alias sync="sudo emerge --sync"
alias update="sudo emerge -aUDu --quiet-build=y @world && flatpak --user update"
alias install="sudo emerge --ask --autounmask --autounmask-write --autounmask-continue --quiet-build=y"
alias remove="sudo emerge --deselect"
alias clean="sudo emerge --depclean"
alias search="emerge -s"
alias rebuild="sudo emerge --exclude=sys-kernel/gentoo-kernel-bin --quiet-build=y @module-rebuild && sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias shutdown="sudo shutdown now"
alias poweroff="sudo poweroff"
alias reboot="sudo reboot"
alias ls="lsd"
alias l="lsd -l"
alias la="lsd -a"
alias lla="lsd -la"
alias lt="lsd --tree"
alias minfetch="sh /home/dasprii/Documents/scripts/minfetch/sysinfo.sh"
#alias off="clear && xset dpms force off"
alias ds4bat="cat '/sys/class/power_supply/sony_controller_battery_1c:a0:b8:46:0c:22/capacity'"
alias mon2cam="deno run --unstable -A -r -q https://raw.githubusercontent.com/ShayBox/Mon2Cam/master/src/mod.ts"

### PATHS ###
path+=/home/dasprii/.deno/bin

