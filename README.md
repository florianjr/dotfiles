# dotfiles

I upload most of my configs here. They may change over time.

[DWM Screenshot](https://i.imgur.com/5yas3It.png)

[DWM Empty Workspace](https://i.imgur.com/ri84m0C.png)

Screenshot Tool: Flameshot

Terminal: kitty

OS: Gentoo 

GTK: Arc-Dark

WM: [DWM](https://dwm.suckless.org/)

Shell: zsh

Shell Theme: [Jovial](https://github.com/zthxxx/jovial)

Colorscheme: [Dracula](https://draculatheme.com/)

I manage this dotfiles repo with [dotbare](https://github.com/kazhala/dotbare)
