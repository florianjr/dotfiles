#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch bar1 and bar2
echo "---" | tee -a /tmp/polybar1.log /tmp/polybar2.log # /tmp/polybar3.log
polybar das >>/tmp/polybar1.log 2>&1 &
polybar prii >>/tmp/polybar2.log 2>&1 &
# polybar three >>/tmp/polybar3.log 2>&1 &
