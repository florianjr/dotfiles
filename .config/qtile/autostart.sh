#! /bin/bash 

function run {
  if ! pgrep -f $1 ;
  then
    $@& 
  fi  
}

run picom -b  
run nitrogen --restore 
run flameshot 
run gwe --hide-window 
run thunar --daemon 
run nmcli connection up arch 
run nm-applet 
run blueman-applet 
run dunst 
run lxpolkit 
run discord 
run spotify 
run termite -e gotop 
