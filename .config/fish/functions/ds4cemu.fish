# Defined in - @ line 1
function ds4cemu --wraps='python3 -m ds4drv --hidraw --udp' --description 'alias ds4cemu python3 -m ds4drv --hidraw --udp'
  python3 -m ds4drv --hidraw --udp $argv;
end
