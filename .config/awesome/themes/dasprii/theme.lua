-----------------------------
-- dasprii's awesome theme --
-----------------------------

-- Imports
local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local gfs = require("gears.filesystem")

-- Paths
local themes_path = ("/home/dasprii/.config/awesome/themes/")
local theme = {}

-- Wallpaper
theme.wallpaper = themes_path.."dasprii/background.png"

-- Font
theme.font          = "ubuntu mono 10"

-- Colors
theme.bg_normal     = "#2e3440"
theme.bg_focus      = "#5e81ac"
theme.bg_urgent     = "#bf616a"
theme.bg_minimize   = "#4c566a"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#d8dee9"
theme.fg_focus      = "#eceff4"
theme.fg_urgent     = "#eceff4"
theme.fg_minimize   = "#d8dee9"

-- Borders
theme.useless_gap   = 7
theme.border_width  = 2
theme.border_normal = "#4c566a"
theme.border_focus  = "#5e81ac"
theme.border_marked = "#bf616a"

-- Taglist 
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Notifications
theme.notification_font			= theme.font
theme.notification_bg			= theme.bg_normal
theme.notification_fg			= theme.fg_focus
theme.notification_max_width		= dpi(250)
theme.notification_max_height		= dpi(125)
theme.notification_icon_size		= dpi(75)

-- Layout Icons
theme.layout_fairh = themes_path.."dasprii/layouts/fairhw.png"
theme.layout_fairv = themes_path.."dasprii/layouts/fairvw.png"
theme.layout_floating  = themes_path.."dasprii/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."dasprii/layouts/magnifierw.png"
theme.layout_max = themes_path.."dasprii/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."dasprii/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."dasprii/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."dasprii/layouts/tileleftw.png"
theme.layout_tile = themes_path.."dasprii/layouts/tilew.png"
theme.layout_tiletop = themes_path.."dasprii/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."dasprii/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."dasprii/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."dasprii/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."dasprii/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."dasprii/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."dasprii/layouts/cornersew.png"

return theme
