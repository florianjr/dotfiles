"   GENERAL SETTINGS
syntax on

set number

set autoindent

set laststatus=2

set noshowmode


"   PLUGINS
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'https://github.com/ap/vim-css-color.git'

Plug 'https://github.com/itchyny/lightline.vim.git'

Plug 'https://github.com/dracula/vim.git'

call plug#end()


"   STATUSLINE COLORSCHEME
let g:lightline = {
  \ 'colorscheme': 'dracula',
  \ }


